/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.borjabravo.readmoretextview;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Text;
import ohos.agp.text.RichTextBuilder;
import ohos.agp.text.TextForm;
import ohos.agp.utils.Color;
import ohos.app.Context;

public class ReadMoreTextView extends Text {
    private static final int DEFAULT_LENGTH_MAX = 50;

    private boolean mIsShowMore = false;
    private String mContent;
    private String mTextLess;
    private String mTextMore;
    private int mColorLess = -1;
    private int mColorMore = -1;
    private int mContentColor = -1;
    private int mContentLength = -1;
    private int mContentSize = -1;


    public ReadMoreTextView(Context context) {
        super(context);
        init();
    }

    public ReadMoreTextView(Context context, AttrSet attrs) {
        super(context, attrs);
        if (attrs.getAttr("text_less").isPresent()) {
            mTextLess = attrs.getAttr("text_less").get().getStringValue();
        }
        if (attrs.getAttr("text_more").isPresent()) {
            mTextMore = attrs.getAttr("text_more").get().getStringValue();
        }
        if (attrs.getAttr("color_less").isPresent()) {
            mColorLess = attrs.getAttr("color_less").get().getColorValue().getValue();
        }
        if (attrs.getAttr("color_more").isPresent()) {
            mColorMore = attrs.getAttr("color_more").get().getColorValue().getValue();
        }
        if (attrs.getAttr("content_color").isPresent()) {
            mContentColor = attrs.getAttr("content_color").get().getColorValue().getValue();
        }
        if (attrs.getAttr("content_max_length").isPresent()) {
            mContentLength = attrs.getAttr("content_max_length").get().getIntegerValue();
        }
        if (attrs.getAttr("content_size").isPresent()) {
            mContentSize = attrs.getAttr("content_size").get().getIntegerValue();
        }
        init();
    }

    public ReadMoreTextView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init();
    }

    private void init() {
        setMultipleLine(true);
        if (Utils.isEmpty(mTextLess)) {
            mTextLess = getContext().getString(ResourceTable.String_read_less);
        }
        if (Utils.isEmpty(mTextMore)) {
            mTextMore = getContext().getString(ResourceTable.String_read_more);
        }
        if (mColorLess == -1) {
            mColorLess = Color.BLUE.getValue();
        }
        if (mColorMore == -1) {
            mColorMore = Color.RED.getValue();
        }
        if (mContentColor == -1) {
            mContentColor = Color.BLACK.getValue();
        }
        if (mContentLength == -1) {
            mContentLength = DEFAULT_LENGTH_MAX;
        }
        if (mContentSize == -1) {
            mContentSize = getTextSize();
        }
    }


    /**
     * setContent
     *
     * @param text text
     */
    public void setContent(String text) {
        mContent = text;
        if (text.length() > mContentLength) {
            mIsShowMore = true;
            changeTextStyle();
        } else {
            setText(text);
            setTextSize(mContentSize);
        }
    }

    private void changeTextStyle() {
        RichTextBuilder builder = new RichTextBuilder();
        String content;
        String tail;
        int color;
        if (mIsShowMore) {
            if (mContent.length() > mContentLength) {
                content = mContent.substring(0, mContentLength) + "...";
            } else {
                content = mContent;
            }
            tail = mTextMore;
            color = mColorMore;
        } else {
            content = mContent;
            tail = mTextLess;
            color = mColorLess;
        }
        TextForm contentForm = new TextForm();
        contentForm.setTextColor(mContentColor);
        contentForm.setTextSize(mContentSize);
        builder.mergeForm(contentForm);
        builder.addText(content);
        TextForm tailForm = new TextForm();
        tailForm.setTextColor(color);
        tailForm.setTextSize(mContentSize);
        builder.mergeForm(tailForm);
        builder.addText(tail);
        setRichText(builder.build());
        setClickedListener(component -> {
            setIClickedListener();
        });
    }

    private void setIClickedListener() {
        mIsShowMore = !mIsShowMore;
        changeTextStyle();
    }


}
