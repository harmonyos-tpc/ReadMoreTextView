# ReadMoreTextView

带有修剪文本的自定义Text

## 集成

### 方案一

```
添加har包到lib文件夹内
在entry的gradle内添加如下代码
implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
```

### 方案二

```
implementation project(':library')
```

### 方案三

```
implementation 'io.openharmony.tpc.thirdlib:ReadMoreTextView:1.0.2'
```

## 使用

要在应用程序上使用ReadMoreTextView，请将以下代码添加到布局中：

```xml
<com.borjabravo.readmoretextview.ReadMoreTextView
    ohos:id="@+id/text_view"
    ohos:layout_width="match_parent"
    ohos:layout_height="wrap_content"/>
```

您可以使用自定义ReadMoreTextView：

- app:trimExpandedText: 展开视图时显示的文本。
- app:trimCollapsedText: 折叠视图时显示的文本。
- app:trimLength: 修剪长度以确定何时显示可单击文本。
- app:showTrimExpandedText: 如果为真，则显示修剪文本。
- app:colorClickableText: 修剪可点击文本的文本颜色。
- app:trimMode: 使用两个选项选择修剪模式：修剪模式长度或修剪模式线。

## 快照

![image](https://gitee.com/openharmony-tpc/ReadMoreTextView/blob/master/gif/readmoretext.gif)

License
=======

    Copyright 2016 Borja Bravo

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
